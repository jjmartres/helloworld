# HelloWorld

```console
$ make help
build                          Build docker images
clean                          Stop and delete inages
dive                           Explore docker image layer of application
doc                            Generate diagram
help                           Show this help
run                            Run application using docker images
scan                           Inline scan of application container using anchore
test                           Run application tests, both ut and it
```

## Requirements

 - Python 3.8.10+
 - Docker 19+
 - Kustomize

## Design and code a simple "Hello World" application that exposes HTTP-based APIs

Have a look a the app folder.

```console 
$ make run 
$ curl http://localhost:8080/hello/npotesa

*   Trying 127.0.0.1:8080...
* TCP_NODELAY set
* Connected to localhost (127.0.0.1) port 8080 (#0)
> GET /hello/npotesa HTTP/1.1
> Host: localhost:8080
> User-Agent: curl/7.68.0
> Accept: */*
> 
* Mark bundle as not supporting multiuse
< HTTP/1.1 200 OK
< date: Wed, 06 Oct 2021 08:34:37 GMT
< server: uvicorn
< content-length: 60
< content-type: application/json
< 
* Connection #0 to host localhost left intact
{"message":"Hello, npotesa! Your birthday is in 364 day(s)"}
```

### Install requirements

```console
pip install -r app/src/requirements.txt
pip install -r app/ut/requirements.txt
```

#### Application environment variables used

| __VAR__ | __DESCRIPTION__ | DEFAULT |
|:-------:|:----------------|:-------:|
| `HELLO_WORLD_SERVER_RELOAD` | Enable or not auto-reload for development | `False` |
| `HELLO_WORLD_SERVER_PORT` | Bind to a socket with this port | `8080` |
| `HELLO_WORLD_DB_HOST` | Database hostname | `n/a` |
| `HELLO_WORLD_DB_PORT` | Database port | `5432` |
| `HELLO_WORLD_DB_NAME` | Database name | `n/a` |
| `HELLO_WORLD_DB_USER` | Database username | `n/a` |
| `HELLO_WORLD_DB_PASSWORD` | Database username password | `n/a` |

### Run server

#### Using Pythoon interpreter

```console
cd app/src
python main.py
```

### Tests

```console
make run 
make test
```

![tests](_docs/tests.png)


### Swagger

[http://localhost:8080/docs](http://localhost:8080/docs)


## Produce a system diagram of your solution deployed to either AWS or GCP (it's not required to support both cloud platform)

![system diagram to GCP](_docs/diagram.png)

Have a look a the diagrams folder for more details.

## Write configuration scripts for building and no-downtime production deployment of this application, keeping in mind aspects that an SRE would have to consider.

Have a look a the deployment folder, but also on the CI/CD (`.gitlab-ci.yaml`).

```console
$ cd deployment/
$ make help
all                            Initialize Kubernetes resourcesthen install database and finally application
application                    Install or upgrade application and rollback if it fails
clean                          Clean all
database                       Install or upgrade postgresql backend and rollback if it fails
dry-run                        Generate manifests only
help                           Show this help
Initialize                     Initialize Kubernetes resources
````

### `.gitlab-ci.yaml`

![gitlab-ci](_docs/cicd.png)
