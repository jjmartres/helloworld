# HelloWorld application deployment

This repository is all resources to deploy HelloWorld application in k8s cluster.

```console
$ make help
all                            Initialize Kubernetes resourcesthen install database and finally application
application                    Install or upgrade application and rollback if it fails
clean                          Clean all
database                       Install or upgrade postgresql backend and rollback if it fails
dry-run                        Generate manifests only
help                           Show this help
Initialize                     Initialize Kubernetes resources
````
