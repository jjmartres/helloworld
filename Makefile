.PHONY: help build run scan dive
.SILENT: clean 

.DEFAULT_GOAL := help

build: ## Build docker images
	@env COMPOSE_PROJECT_NAME="helloworld-$$(git rev-parse --abbrev-ref HEAD)"  BUILD_TIMESTAMP="$$(date -u +%FT%H:%M:%SZ)" docker-compose build
	
run: ## Run application using docker images
	@COMPOSE_PROJECT_NAME="helloworld-$$(git rev-parse --abbrev-ref HEAD)" BUILD_TIMESTAMP="$$(date -u +%FT%H:%M:%SZ)" docker-compose up --quiet-pull -d

test: run  ## Run application tests, both ut and it
	@cd app && pytest ut/ && pytest it/

scan: ## Inline scan of application container using anchore
	@curl -s https://ci-tools.anchore.io/inline_scan-v$${INLINESCAN_VER:-0.10.0} | bash -s -- -d ./docker/application/Dockerfile helloworld-$$(git rev-parse --abbrev-ref HEAD)_application:$${HELLOWORLD_VER:-latest}

dive: ## Explore docker image layer of application
	@docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock wagoodman/dive:latest helloworld-$$(git rev-parse --abbrev-ref HEAD)_application:$${HELLOWORLD_VER:-latest}

clean: ## Stop and delete inages
	@COMPOSE_PROJECT_NAME="helloworld-$$(git rev-parse --abbrev-ref HEAD)" docker-compose down --rmi all
	docker rmi helloworld* anchore* agoodman/dive 

doc: ## Generate diagram
	@python diagrams/main.py _docs/ png

help: ## Show this help
	@grep -E '^([a-zA-Z_-]+|[a-zA-Z_-]+/[a-zA-Z_-]+):.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
