import random
import re
import psycopg2
import requests
import string
from datetime import date
from config import cfg


_dsn = cfg.database
_cfg = cfg.server

base_url = "http://{}:{}".format(_cfg.server_host, _cfg.server_port)

def test_get_user():
    r = requests.get(f"{base_url}/hello/npotesa")

    assert r.status_code == 200

    message = r.json()["message"]

    date_ = date(1976, 10, 5)

    current_date = date.today()
    if date_.month == current_date.month and date_.day == current_date.day:
        assert message == "Hello, npotesa! Happy birthday !!"
    else:
        assert re.match(r"Hello, npotesa! Your birthday is in \d+ day\(s\)", message)


def test_update_user_birthday():
    date_ = date(1976, 10, 5)

    # assert user does not exists
    with psycopg2.connect(**_dsn.as_dict()) as conn:
        with conn.cursor() as cur:
            name = random_name(10)
            while True:
                cur.execute("SELECT exists(select * from username where name=%(name)s)", {"name": name})
                if not cur.fetchone()[0]:
                    break

    # test create user
    r = requests.put(f"{base_url}/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})

    assert r.status_code == 204
    _assert_user_in_database(name, date_)

    # test update user birthday
    date_ = date(1976, 10, 6)
    r = requests.put(f"{base_url}/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})

    assert r.status_code == 204
    _assert_user_in_database(name, date_)


def _assert_user_in_database(name, date_):
    with psycopg2.connect(**_dsn.as_dict()) as conn:
        with conn.cursor() as cur:
            cur.execute("select * from username where name=%(name)s", {"name": name})
            row = cur.fetchone()
            assert row is not None

            assert row[0] == name
            assert row[1] == date_


def random_name(length):
    name = ''.join((random.choice(string.ascii_letters)
                    for i in range(length))).lower()
    return name
