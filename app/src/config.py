import dataclasses
import os
from dataclasses import dataclass

@dataclass
class Server:
    devmode: bool
    server_port: int
    server_host: str

    def as_dict(self):
        return dataclasses.asdict(self)

@dataclass
class Database:
    host: str
    port: int
    database: str
    user: str
    password: str

    def as_dict(self):
        return dataclasses.asdict(self)

class Config:
    def __init__(self):
        self._server = None
        self._database = None

    @property
    def server(self):
        if self._server is None:
            self._server = Server(
                os.environ.get("HELLO_WORLD_SERVER_RELOAD", "False") == "False",
                int(os.environ.get("HELLO_WORLD_SERVER_PORT", 8080)),
                os.environ.get("HELLO_WORLD_SERVER_HOST", "localhost")
            )
        return self._server

    @property
    def database(self):
        if self._database is None:
            self._database = Database(
            os.environ.get("HELLO_WORLD_DB_HOST", "localhost"),
            int(os.environ.get("HELLO_WORLD_DB_PORT", 5432)),
            os.environ.get("HELLO_WORLD_DB_DATABASE", "helloworld_d"),
            os.environ.get("HELLO_WORLD_DB_USER", "helloworld_u"),
            os.environ.get("HELLO_WORLD_DB_PASSWORD", "helloworld_p")
            )

        return self._database

cfg = Config()
