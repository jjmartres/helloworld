import re
from datetime import date


class InvalidDateOfBirth(Exception):
    def __init__(self, dateofbirth, msg=None):
        if msg is None:
            msg = "The date of birth date %s is invalid" % dateofbirth
        super(InvalidDateOfBirth, self).__init__(msg)
        self.date_of_birth = dateofbirth


class InvalidUsername(Exception):
    def __init__(self, username, msg=None):
        if msg is None:
            msg = "Username %s must contain only letters" % username
        super(InvalidUsername, self).__init__(msg)
        self.username = username


class Username(object):
    letter_regex = re.compile('^[A-Za-z]*$')

    def __init__(self, name, birthday):
        self._name = name
        self._birthday = birthday

    @property
    def name(self):
        return self._name

    @property
    def birthday(self):
        return self._birthday

    def set_birthday(self, date_):
        if not self._date_is_under_current_date(date_):
            raise InvalidDateOfBirth(date_)

        self._birthday = date_

    def is_anniversary(self):
        current_date = get_current_date()
        return current_date.month == self._birthday.month and current_date.day == self._birthday.day

    def days_to_anniversary(self):
        current_date = get_current_date()
        if current_date.month < self._birthday.month or (current_date.month == self._birthday.month and current_date.day <= self._birthday.day):
            next_anniversary_date = date(current_date.year, self._birthday.month, self._birthday.day)
        else:
            next_anniversary_date = date(current_date.year + 1, self._birthday.month, self._birthday.day)

        return (next_anniversary_date - current_date).days

    @staticmethod
    def _user_should_have_only_letters(name: str):
        return Username.letter_regex.match(name)

    @staticmethod
    def _date_is_under_current_date(date_: date):
        return date_ < get_current_date()

    @staticmethod
    def create(name: str, date_of_birth: date):
        if not Username._date_is_under_current_date(date_of_birth):
            raise InvalidDateOfBirth(date_of_birth)

        if not Username._user_should_have_only_letters(name):
            raise InvalidUsername(name)

        return Username(name, date_of_birth)


def get_current_date():
    return date.today()
