import uvicorn
from config import cfg


_cfg = cfg.server


if __name__ == "__main__":
    uvicorn.run("server:app", host="0.0.0.0", port=_cfg.server_port, reload="_cfg.devmode")
