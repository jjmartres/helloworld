import psycopg2
from config import cfg
from user import Username

_dsn = cfg.database


def cursor_session(fn):
    def f(*args):
        with psycopg2.connect(**_dsn.as_dict()) as conn:
            with conn.cursor() as cur:
                return fn(cur, *args)

    return f


@cursor_session
def get_user(cursor, name):
    # Check if a username already exist in database
    query = "select name, birthday FROM username where name = %(name)s"
    cursor.execute(query, {"name": name})
    row = cursor.fetchone()
    if not row:
        return None

    return Username(name=row[0], birthday=row[1])


@cursor_session
def create_user(cursor, user):
    # Create or update username entry
    query = "INSERT INTO username (name, birthday) VALUES(%(name)s, %(birthday)s)"

    cursor.execute(query, {"name": user.name, "birthday": user.birthday})


@cursor_session
def update_user(cursor, user):
    # Create or update username entry
    query = "UPDATE username set birthday = %(birthday)s where name= %(name)s"

    cursor.execute(query, {"name": user.name, "birthday": user.birthday})
