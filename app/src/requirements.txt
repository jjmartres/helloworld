pip==21.2.4
uvicorn==0.15.0
fastapi==0.68.1
psycopg2-binary==2.9.1