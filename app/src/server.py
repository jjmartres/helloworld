from datetime import date

from fastapi import FastAPI, HTTPException, Body
from fastapi import Response
from pydantic import BaseModel
from starlette.status import HTTP_204_NO_CONTENT, HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_404_NOT_FOUND

import db
from user import Username, InvalidDateOfBirth, InvalidUsername

app = FastAPI()


@app.get("/internal/liveness", status_code=HTTP_200_OK)
@app.get("/internal/readyness", status_code=HTTP_200_OK)
@app.get("/", status_code=HTTP_200_OK)
# Keep this for the Google Cloud Load-Balancer to bring and stay up
async def read_main():
    return {"message": "Welcome to Hello World app."}


@app.get("/hello/{username}")
# Check if user exists
async def get_user(username: str):
    user = db.get_user(username)
    if not user:
        raise HTTPException(status_code=HTTP_404_NOT_FOUND, detail="User doesn't exist")

    if user.is_anniversary():
        return {"message": "Hello, {name}! Happy birthday !!".format(name=username)}
    else:
        anniversary_delta = user.days_to_anniversary()
        return {"message": "Hello, {name}! Your birthday is in {delta} day(s)".format(name=username,
                                                                                      delta=anniversary_delta)}


class UsernameUpdateModel(BaseModel):
    date_of_birth: date = Body(None, alias="dateOfBirth")


@app.put("/hello/{username}", status_code=HTTP_204_NO_CONTENT)
async def create_or_update_username(username: str, username_update_model: UsernameUpdateModel):
    user = db.get_user(username)
    if user is None:
        _create_user(username, username_update_model.date_of_birth)
    else:
        _update_user(user, username_update_model.date_of_birth)

    return Response(status_code=HTTP_204_NO_CONTENT)


def _create_user(name, birthday):
    try:
        user = Username.create(name, birthday)
    except InvalidUsername:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST, detail="Username must contain only letters")

    except InvalidDateOfBirth:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST,
                            detail="The date of birth must must be a date before the today date.")

    db.create_user(user)


def _update_user(user, birthday):
    try:
        user.set_birthday(birthday)
    except InvalidDateOfBirth:
        raise HTTPException(status_code=HTTP_400_BAD_REQUEST,
                            detail="The date of birth must must be a date before the today date.")

    db.update_user(user)
