from fastapi.testclient import TestClient
from datetime import date
from unittest.mock import patch, MagicMock, Mock
from server import app
from user import InvalidDateOfBirth, InvalidUsername

client = TestClient(app)


def test_liveness_probe():
    response = client.get("/internal/liveness")
    assert response.status_code == 200


def test_readyness_probe():
    response = client.get("/internal/readyness")
    assert response.status_code == 200


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Welcome to Hello World app."}


@patch('db.get_user')
def test_get_username_anniversary(get_user):
    username = "himesonl"
    user_mock = MagicMock()
    user_mock.name.return_value = username
    user_mock.is_anniversary.return_value = True

    get_user.return_value = user_mock
    response = client.get("/hello/himesonl")
    assert response.status_code == 200
    assert response.json() == {
        "message": "Hello, {name}! Happy birthday !!".format(name=username)}


@patch('db.get_user')
def test_get_username_days_to_anniversary(get_user):
    username = "himesonl"
    anniversary_delta = 10
    user_mock = MagicMock()
    user_mock.name.return_value = username
    user_mock.days_to_anniversary.return_value = anniversary_delta
    user_mock.is_anniversary.return_value = False

    get_user.return_value = user_mock
    response = client.get("/hello/himesonl")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello, {name}! Your birthday is in {delta} day(s)".format(name=username,
                                                                                                    delta=anniversary_delta)}


@patch('db.get_user')
def test_get_username_not_exists(get_user):
    get_user.return_value = None

    response = client.get("/hello/himesonl")
    assert response.status_code == 404
    assert response.json() == {"detail": "User doesn't exist"}


@patch('db.create_user')
@patch('db.get_user')
def test_create_username(get_user, create_user):
    get_user.return_value = None
    create_user.return_value = None

    name = "himesonl"
    date_ = date(2000, 1, 10)
    response = client.put(
        f"/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})
    assert response.status_code == 204


@patch('user.Username.create')
@patch('db.create_user')
@patch('db.get_user')
def test_create_username_invalid_name(get_user, create_user, create):
    get_user.return_value = None
    create_user.return_value = None
    date_ = date(2000, 1, 10)
    create.side_effect = Mock(side_effect=InvalidUsername(date_))

    name = "himesonl3"
    response = client.put(
        f"/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})
    assert response.status_code == 400
    assert response.json() == {"detail": "Username must contain only letters"}


@patch('user.Username.create')
@patch('db.create_user')
@patch('db.get_user')
@patch('user.get_current_date')
def test_create_username_invalid_date(get_current_date, get_user, create_user, create):
    get_current_date.return_value = date(2021, 12, 2)
    get_user.return_value = None
    create_user.return_value = None

    date_ = date(2022, 1, 10)
    create.side_effect = Mock(side_effect=InvalidDateOfBirth(date_))

    name = "himesonl"
    response = client.put(
        f"/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})
    assert response.status_code == 400
    assert response.json() == {
        "detail": "The date of birth must must be a date before the today date."}


@patch('db.update_user')
@patch('db.get_user')
@patch('user.get_current_date')
def test_update_username_invalid_date(get_current_date, get_user, update_user):
    get_current_date.return_value = date(2021, 12, 2)
    date_ = date(2022, 1, 10)

    user = MagicMock()
    user.set_birthday.side_effect = Mock(side_effect=InvalidDateOfBirth(date_))

    get_user.return_value = user
    update_user.return_value = None

    name = "himesonl"
    response = client.put(
        f"/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})
    assert response.status_code == 400
    assert response.json() == {
        "detail": "The date of birth must must be a date before the today date."}


@patch('db.update_user')
@patch('db.get_user')
@patch('user.get_current_date')
def test_update_username(get_current_date, get_user, update_user):
    get_current_date.return_value = date(2021, 12, 2)
    date_ = date(2022, 1, 10)

    user = MagicMock()
    user.set_birthday.return_value = None

    get_user.return_value = user
    update_user.return_value = None

    name = "himesonl"
    response = client.put(
        f"/hello/{name}", json={"dateOfBirth": date_.strftime("%Y-%m-%d")})
    assert response.status_code == 204
