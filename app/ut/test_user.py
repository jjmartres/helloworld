import pytest
from datetime import date, timedelta
from unittest.mock import patch
from user import Username
from user import InvalidDateOfBirth
from user import InvalidUsername


def test_set_birthday():
    user = Username.create("johndoe", date(1970, 12, 3))

    birthday = date(1970, 12, 2)
    user.set_birthday(birthday)

    assert user.birthday == birthday


def test_set_birthday_invalid():
    user = Username.create("johndoe", date(1970, 12, 3))

    birthday = date.today() + timedelta(days=1)

    with pytest.raises(InvalidDateOfBirth):
        user.set_birthday(birthday)


def test_anniversary():
    user = Username.create("johndoe", date(1970, 12, 3))
    assert not user.is_anniversary()

    today = date.today()
    user = Username.create("johndoe", date(1970, today.month, today.day))
    assert user.is_anniversary()


@patch('user.get_current_date')
def test_days_to_anniversary(get_current_date):
    get_current_date.return_value = date(2021, 12, 2)
    user = Username.create("johndoe", date(1970, 12, 3))
    assert user.days_to_anniversary() == 1

    user = Username.create("johndoe", date(1970, 12, 2))
    assert user.days_to_anniversary() == 0

    user = Username.create("johndoe", date(1970, 12, 1))
    assert user.days_to_anniversary() == 364


def test_create_user():
    birthday = date(1970, 12, 3)
    name = "johndoe"
    user = Username.create(name, birthday)
    assert user.name == name
    assert user.birthday == birthday


def test_create_user_invalid_name():
    with pytest.raises(InvalidUsername):
        Username.create("john3doe", date(1970, 12, 1))


def test_create_user_invalid_birthday():
    with pytest.raises(InvalidDateOfBirth):
        Username.create("johndoe", date.today() + timedelta(days=1))
