import os
import sys

from datetime import datetime
from diagrams import Diagram, Cluster, Edge
from diagrams.onprem.client import Users
from diagrams.gcp.network import Armor, DNS
from diagrams.k8s.clusterconfig import HPA
from diagrams.k8s.compute import Deployment, Pod, StatefulSet
from diagrams.k8s.network import Ingress, Service
from diagrams.k8s.podconfig import Secret
from diagrams.k8s.storage import PV, PVC, StorageClass

name="HelloWorld"

now = datetime.now()
dt_string = now.strftime("%d/%m/%Y %H:%M:%S %p")

## diagram attributes
graph_attr = {
    "layout": "dot",
    "fontsize": "24",
    "constraint": "True"
}

edge_attr = {
    "splines": "polyline",
    "concentrate": "True"
}

## custom ColorMap
## from https://graphviz.org/doc/info/colors.html
linkColorMap = {
    "config": "grey",
    "k8ssvc": "firebrick",
    "pubsvc": "dodgerblue",
    "inbound": "darkgreen",
    "users": "indigo"
}

## edge config
def configLink():
    return Edge(color=linkColorMap["config"], style="dashed", penwidth="2")

def k8sSvcLink():
    return Edge(color=linkColorMap["k8ssvc"], style="fine", penwidth="1")

def pubSvcLink():
    return Edge(color=linkColorMap["pubsvc"], style="fine", penwidth="1")

def inboundLink():
    return Edge(color=linkColorMap["inbound"], style="fine", penwidth="1")

def usersLink():
    return Edge(color=linkColorMap["users"], style="fine", penwidth="1")

## diagram legend
diagram_label = f"\nSystem diagram of {name} solution deployed to GCP\nLast updated: {dt_string}"

## Generate diagram
with Diagram(diagram_label, show=False, direction="LR", graph_attr=graph_attr, edge_attr=edge_attr, outformat=f"{sys.argv[2]}", filename=f"{sys.argv[1]}/diagram"):

    appUsers = Users(f"Application users")

    with Cluster(f"Google Cloud Platform:  project {name}"):
        cloudArmor = Armor(f"CloudArmor")
        entryPoint = DNS(f"hello.world.tld")

        with Cluster(f"Kubernetes cluster: k8s001"):
            with Cluster(f"Namespace: database"):

                with Cluster("database"):
                    db_svc = Service(f"database")
                    db_svc >> Pod(f"database-0") >> StatefulSet(f"database") >> PVC(f"data-database-0") << PV(
                        "PersistentVolume") << StorageClass("standard")

            with Cluster(f"Namespace: application"):
                gitlab_registry = Secret(f"gitlab-docker-registry")

                appconfig = Secret(f"appconfig")

                with Cluster("application"):
                    app_ing = Ingress(f"ingress")

                    app_svc = Service(f"app")
                    app = []
                    app_pod = Pod(f"app")
                    app_pod - configLink() - appconfig
                    app.append(app_pod << Deployment("app") << HPA("app"))
                    app >> k8sSvcLink() >> db_svc
                   
                    entryPoint >> inboundLink() >> app_ing >> app_svc


    appUsers >> usersLink() >> cloudArmor >> inboundLink() >> entryPoint




