# Diagrams

Please see [diagrams web site](https://diagrams.mingrammer.com/) for further reference

## Genererate diagrams

### PDF format

```coonsole
python main.py . pdf
```
### PNG format

```coonsole
python main.py . png


